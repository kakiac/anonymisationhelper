In word 2010

Step 1. Install the macros 
1.	From the Developer ribbon > Visual Basic
2.	Make sure you are editing the �normal.dotm� file (check the tree on the left)
3.	Copy the text from the anonymisation_helper_macros_full.txt file to the end of the file
4.	Save and exit the Visual basic editor

Step 2. Install the ribbon
1.	From File >  Options > Customise Ribbon > Import/Export (right bottom of the screen) > Import customisation file
2.	Find the AnonymisationHelperTab_00_02.xml
3.	Click open
4.	A new Tab should have appeared in your list of tabs. Click ok/ back to see the ribbon in your word environment
